# dhus products downloader

import os
import base64
import requests
from tqdm import tqdm
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree


def download_product(product_url):
    product_id = product_url.split("'")[1] + '.zip'

    with requests.get(product_url, stream=True, auth=HTTPBasicAuth('alexm98', base64.b64decode('anVzdHNvbWVyYW5kb21wYXNzMTIz'))) as r:
        r.raise_for_status()
        with open('dhus_products/' + product_id, 'wb') as fd:
            for chunk in r.iter_content(chunk_size=2048):
                if chunk:
                    fd.write(r.content)
                    fd.flush()
                    os.fsync(fd.fileno())


class OpenSearchLoader:
    def __init__(self, url, parameters):
        self._current_position = -1
        self._nr_products = -1
        self._product_urls = []
        self._get_data(url, parameters)

    def _get_data(self, url, parameters):
        print(f"Products No. - {self._nr_products}, URL list - {len(self._product_urls)}")

        if len(self._product_urls) == self._nr_products:
            return None

        else:
            self._results = requests.get(url, params=parameters, auth=HTTPBasicAuth('alexm98', base64.b64decode('anVzdHNvbWVyYW5kb21wYXNzMTIz')))

            if self._results.status_code == 200:
                # with open('results.xml', 'wb') as f:
                #     f.write(self._results.content)

                self._results = xml.etree.ElementTree.fromstring(self._results.content)
                self._nr_products = int(self._results.find('{http://a9.com/-/spec/opensearch/1.1/}totalResults').text)
                self._product_urls = self._product_urls + [prod[1].get('href') for prod in self._results.findall('{http://www.w3.org/2005/Atom}entry')]
                parameters['start'] = parameters['start'] + 100
                self._get_data(url, parameters)
            else:
                print(self._results.status_code)
                raise requests.exceptions.HTTPError

    def reset(self):
        self._curent_position = 0

    def __len__(self):
        return len(self._product_urls)

    def __next__(self):
        length = len(self._product_urls)
        if length == 0:
            raise StopIteration()

        self._current_position += 1
        if(self._current_position < length):
            return self._product_urls[self._current_position]
        else:
            raise StopIteration()

    def __iter__(self):
        return self


# maybe set cloud coverage at a low? -> Not good, only images with cloudcoverage from 0 to 1.5 are either desert or ocean images.
# all Europe AOI: (POLYGON((-13.635369817737987 35.99070424440009,29.91214635409535 35.99070424440009,29.91214635409535 63.9526816845281,-13.635369817737987 63.9526816845281,-13.635369817737987 35.99070424440009)))

# (POLYGON((16.58910503349143 43.400842665330345,26.95841113834191 43.400842665330345,26.95841113834191 49.09541206485471,16.58910503349143 49.09541206485471,16.58910503349143 43.400842665330345)))

results = OpenSearchLoader('https://scihub.copernicus.eu/dhus/search', {'q': 'beginPosition:[2018-06-01T00:00:00.000Z TO 2018-09-01T23:59:59.999Z] AND endPosition:[2018-06-01T00:00:00.000Z TO 2018-09-01T23:59:59.999Z] AND producttype: S2MSI1C AND cloudcoverpercentage:[0 TO 1.5] AND (platformname:Sentinel-2) AND footprint: "Intersects (POLYGON((16.58910503349143 43.400842665330345,26.95841113834191 43.400842665330345,26.95841113834191 49.09541206485471,16.58910503349143 49.09541206485471,16.58910503349143 43.400842665330345)))"', 'rows': 100, 'start': 0})

# https://scihub.copernicus.eu/dhus/search?q=beginPosition:[2018-06-01T00:00:00.000Z TO 2018-09-01T23:59:59.999Z] AND endPosition:[2018-06-01T00:00:00.000Z TO 2018-09-01T23:59:59.999Z] AND producttype: S2MSI1C AND cloudcoverpercentage:[0 TO 1.5] AND (platformname:Sentinel-2) AND footprint: "Intersects(POLYGON((-13.635369817737987 35.99070424440009,29.91214635409535 35.99070424440009,29.91214635409535 63.9526816845281,-13.635369817737987 63.9526816845281,-13.635369817737987 35.99070424440009)))"&rows=100&start=0

pbar = tqdm(results)
for result in pbar:
    pbar.set_description(f"Downloading {result}.zip")
    download_product(result)
