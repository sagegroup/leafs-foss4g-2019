function maskS2clouds(image) {
    var qa = image.select('QA60');

    // Bits 10 and 11 are clouds and cirrus, respectively.
    var cloudBitMask = 1 << 10;
    var cirrusBitMask = 1 << 11;

    // Both flags should be set to zero, indicating clear conditions.
    var mask = qa.bitwiseAnd(cloudBitMask).eq(0).and(qa.bitwiseAnd(cirrusBitMask).eq(0));

    return image.updateMask(mask).divide(10000);
}

// Map the function over one year of data and take the median.
// Load Sentinel-2 TOA reflectance data.
var dataset = ee.ImageCollection('COPERNICUS/S2')
    .filterDate('2018-07-01', '2018-09-01')
    .filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 20))
    .select(['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B9', 'B10', 'B11', 'B12'])
    .limit(10);
    // .map(maskS2clouds); // apply cloud mask

var geometries = [ee.Geometry.Polygon([[[16, 44], [18.5, 44], [18.5, 45.2], [16, 45.2]]]),
		  ee.Geometry.Polygon([[[18.5, 44], [21.5, 44], [21.5, 45.2], [18.5, 45.2]]]),
		  ee.Geometry.Polygon([[[21.5, 44], [24.25, 44], [24.25, 45.2], [21.5, 45.2]]]),
		  ee.Geometry.Polygon([[[24.25, 44], [27.44, 44], [27.44, 45.2], [24.25, 45.2]]]),
		  ee.Geometry.Polygon([[[16,45.2], [18.5,45.2], [18.5, 46.4], [16, 46.4]]]),
		  ee.Geometry.Polygon([[[18.5,45.2], [21.5,45.2], [21.5, 46.4], [18.5, 46.4]]]),
		  ee.Geometry.Polygon([[[21.5,45.2], [24.25,45.2], [24.25, 46.4], [21.5, 46.4]]]),
		  ee.Geometry.Polygon([[[24.25,45.2], [27.44,45.2], [27.44, 46.4], [24.25, 46.4]]]),
		  ee.Geometry.Polygon([[[16,46.4], [18.5,46.4], [18.5, 47.6], [16, 47.6]]]),
		  ee.Geometry.Polygon([[[18.5,46.4], [21.5,46.4], [21.5, 47.6], [18.5, 47.6]]]),
		  ee.Geometry.Polygon([[[21.5,46.4], [24.25,46.4], [24.25, 47.6], [21.5, 47.6]]]),
		  ee.Geometry.Polygon([[[24.25,46.4], [27.44,46.4], [27.44, 47.6], [24.25, 47.6]]])];


var colors = ['FFF000', 'FF0000', '00FF00', '00FFFF', '0F0F0F', '0AFF0F', 'AAA00F', 'FFFFFF'];

for(var geometry in geometries){
    // var batch = require('users/fitoprincipe/geetools:batch');
    // batch.Download.ImageCollection.toDrive(dataset, "forestry-segmentation", {scale:10, region:geometry, maxPixels: 1e13, formatOptions: {cloudOptimized: true}});
    Map.addLayer(geometries[geometry], {color: colors[geometry]}, '');
}
