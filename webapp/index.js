import Map from 'ol/Map.js';
import View from 'ol/View.js';
import {getWidth, getTopLeft} from 'ol/extent.js';
// import TileLayer from 'ol/layer/Tile.js';
import {get as getProjection} from 'ol/proj.js';
import OSM from 'ol/source/OSM.js';
import TileWMS from 'ol/source/TileWMS';
import ExtentInteraction from 'ol/interaction/Extent.js';
import {platformModifierKeyOnly} from 'ol/events/condition.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';


// imports for measuring tool
import {unByKey} from 'ol/Observable.js';
import Overlay from 'ol/Overlay.js';
import {getArea, getLength} from 'ol/sphere.js';
import {Vector as VectorSource} from 'ol/source.js';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style.js';
import Draw from 'ol/interaction/Draw.js';
import {LineString, Polygon} from 'ol/geom.js';

import Chart from 'chart.js';

var osmlayer = new TileLayer({source: new OSM(), opacity: 0.7});
var mosaic_rgb = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:mosaic_T35_RGB2'}})})
var prediction_mosaic = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:mosaic_T35'}})})
var forest_example = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:AOI_T35TML'}})})

var T35TML_2016 = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:T35_2016_RGB'}})})
var T35TML_2017 = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:T35_2017_RGB'}})})
var T35TML_2018 = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:T35_2018_RGB'}})})

var T35TML_2016_prediction = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:T35TML-20160809T090602'}})})
var T35TML_2017_prediction = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:T35TML-20170918T090609 '}})})
var T35TML_2018_prediction = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:T35TML-20180901T092031 '}})})

var mosaic_t35_tci = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:mosaic_T35_TCI'}})})
var mosaic_t34_tci = new TileLayer({source: new TileWMS({projection: 'EPSG:4326', url: 'https://thor.sage.ieat.ro/geoserver/wms', params: {'LAYERS': 'foss4g:mosaic_T34_TCI'}})})

var source = new VectorSource();

var vector = new VectorLayer({
    source: source,
    style: new Style({
        fill: new Fill({
            color: 'rgba(255, 255, 255, 0.2)'
        }),
        stroke: new Stroke({
            color: '#ffcc33',
            width: 2
        }),
        image: new CircleStyle({
            radius: 7,
            fill: new Fill({
		color: '#ffcc33'
            })
        })
    })
});

var map = new Map({
    // layers: [osmlayer, mosaic_rgb, prediction_mosaic, vector],
    layers: [osmlayer, vector, mosaic_t35_tci,mosaic_t34_tci],
    target: 'map',
    view: new View({
        center: [2800000, 5713697],
        zoom: 6.5
    })
});


var sketch;
var helpTooltipElement;
var helpTooltip;
var measureTooltipElement;
var measureTooltip;
var continuePolygonMsg = 'Click to continue drawing the polygon';
var continueLineMsg = 'Click to continue drawing the line';


var pointerMoveHandler = function(evt) {
    if (evt.dragging) {
        return;
    }
    /** @type {string} */
    var helpMsg = 'Click to start drawing';

    if (sketch) {
        var geom = (sketch.getGeometry());
        if (geom instanceof Polygon) {
	    helpMsg = continuePolygonMsg;
        } else if (geom instanceof LineString) {
	    helpMsg = continueLineMsg;
        }
    }

    helpTooltipElement.innerHTML = helpMsg;
    helpTooltip.setPosition(evt.coordinate);
    helpTooltipElement.classList.remove('hidden');
};


map.on('pointermove', pointerMoveHandler);

map.getViewport().addEventListener('mouseout', function() {
    helpTooltipElement.classList.add('hidden');
});

var typeSelect = document.getElementById('drawtype');
var draw; // global so we can remove it later


/**
 * Format length output.
 * @param {module:ol/geom/LineString~LineString} line The line.
 * @return {string} The formatted length.
 */
var formatLength = function(line) {
    var length = getLength(line);
    var output;
    if (length > 100) {
        output = (Math.round(length / 1000 * 100) / 100) +
            ' ' + 'km';
    } else {
        output = (Math.round(length * 100) / 100) +
            ' ' + 'm';
    }
    return output;
};


/**
 * Format area output.
 * @param {module:ol/geom/Polygon~Polygon} polygon The polygon.
 * @return {string} Formatted area.
 */
var formatArea = function(polygon) {
    var area = getArea(polygon);
    var output;
    if (area > 10000) {
        output = (Math.round(area / 1000000 * 100) / 100) +
            ' ' + 'km<sup>2</sup>';
    } else {
        output = (Math.round(area * 100) / 100) +
            ' ' + 'm<sup>2</sup>';
    }
    return output;
};


function addInteraction() {
    // var type = (typeSelect.value == 'area' ? 'Polygon' : 'LineString');

    switch(typeSelect.value){
    case 'area':
	var type = 'Polygon';
	break;
    case 'length':
	var type = 'LineString';
	break;
    default:
	var type = 'Draw';
    }

    if(type != 'Draw'){
	draw = new Draw({
            source: source,
            type: type,
            style: new Style({
		fill: new Fill({
		    color: 'rgba(255, 255, 255, 0.2)'
		}),
		stroke: new Stroke({
		    color: 'rgba(0, 0, 0, 0.5)',
		    lineDash: [10, 10],
		    width: 2
		}),
		image: new CircleStyle({
		    radius: 5,
		    stroke: new Stroke({
			color: 'rgba(0, 0, 0, 0.7)'
		    }),
		    fill: new Fill({
			color: 'rgba(255, 255, 255, 0.2)'
		    })
		})
            })
	});
	
	map.addInteraction(draw);
	createMeasureTooltip();
	createHelpTooltip();

	var listener;
	draw.on('drawstart',
		function(evt) {
		    // set sketch
		    sketch = evt.feature;

		    /** @type {module:ol/coordinate~Coordinate|undefined} */
		    var tooltipCoord = evt.coordinate;

		    listener = sketch.getGeometry().on('change', function(evt) {
			var geom = evt.target;
			var output;
			if (geom instanceof Polygon) {
			    output = formatArea(geom);
			    tooltipCoord = geom.getInteriorPoint().getCoordinates();
			} else if (geom instanceof LineString) {
			    output = formatLength(geom);
			    tooltipCoord = geom.getLastCoordinate();
			}
			measureTooltipElement.innerHTML = output;
			measureTooltip.setPosition(tooltipCoord);
		    });
		}, this);

	draw.on('drawend',
		function() {
		    measureTooltipElement.className = 'tooltip tooltip-static';
		    measureTooltip.setOffset([0, -7]);
		    // unset sketch
		    sketch = null;
		    // unset tooltip so that a new one can be created
		    measureTooltipElement = null;
		    createMeasureTooltip();
		    unByKey(listener);
		}, this);
    }
}


/**
 * Creates a new help tooltip
 */
function createHelpTooltip() {
    if (helpTooltipElement) {
        helpTooltipElement.parentNode.removeChild(helpTooltipElement);
    }
    helpTooltipElement = document.createElement('div');
    helpTooltipElement.className = 'tooltip hidden';
    helpTooltip = new Overlay({
        element: helpTooltipElement,
        offset: [15, 0],
        positioning: 'center-left'
    });
    map.addOverlay(helpTooltip);
}


/**
 * Creates a new measure tooltip
 */
function createMeasureTooltip() {
    if (measureTooltipElement) {
        measureTooltipElement.parentNode.removeChild(measureTooltipElement);
    }
    measureTooltipElement = document.createElement('div');
    measureTooltipElement.className = 'tooltip tooltip-measure';
    measureTooltip = new Overlay({
        element: measureTooltipElement,
        offset: [0, -15],
        positioning: 'bottom-center'
    });
    map.addOverlay(measureTooltip);
}


/**
 * Let user change the geometry type.
 */
typeSelect.onchange = function() {
    map.removeInteraction(draw);
    addInteraction();
};

addInteraction();

function add(year){
    switch(year){
    case '2016':
	map.removeLayer(T35TML_2016);
	map.removeLayer(T35TML_2017);
	map.removeLayer(T35TML_2018);
	map.addLayer(T35TML_2016);
	map.getView().fit(T35TML_2016.calculateExtent(), map.getSize());
    	break;
    case '2017':
	map.removeLayer(T35TML_2017);
	map.removeLayer(T35TML_2016);
	map.removeLayer(T35TML_2018);
    	map.addLayer(T35TML_2017);	
    	break;
    default:
	map.removeLayer(T35TML_2018);
	map.removeLayer(T35TML_2017);
	map.removeLayer(T35TML_2016);
	map.addLayer(T35TML_2018);
    	break;
    }
}




document.getElementById('T35TML').addEventListener("click", function(){
    for (var i =0; i<map.getLayers.length;i++){
        print(type)

    }
    map.addLayer(T35TML_2018);
    map.addLayer(forest_example);
});

document.getElementById('year2016').addEventListener("click", function(){
    add('2016');
});

document.getElementById('year2017').addEventListener("click", function(){
    add('2017');});

document.getElementById('year2018').addEventListener("click", function(){
    add('2018');});


// statistics charts

var ctx = document.getElementById('chartjs');

var prediction_aoi_chart = new Chart(ctx, {
    type: 'pie',
    data: {
	// 'Agriculture', 'Agro-Forestry', 'Broad leaved forests', 'Coniferous forest', 'Mixed Forest', 'Natural Grasslands', 'Moors and heathland', 'Schlerophylus vegetation', 'Transitional woodlands-shrub'
        labels: ['Agriculture', 'Broad leaved forests', 'Coniferous forest', 'Mixed Forest', 'Natural Grasslands', 'Transitional woodlands-shrub'],
        datasets: [{
            label: 'Occurence of classes',
	    data: [75221, 1802117, 129617, 33375, 200816, 402151],
            backgroundColor: [
		"#e6cc4d",
		"#80ff00",
		"#00a600",
		"#4dff00",
		"#ccf24d",
		"#a6f200",
            ],
        }]
    },
    options: {
	 legend: {
            display: false
         },
	 // title: {
         //    display: true,
         //    text: '2018'
         // }
    }
});


document.getElementById("stats").addEventListener("click", function(){
    var chart = document.getElementById("chartjs");

    if(chart.style.display == "block"){
	chart.setAttribute('style', 'display: none !important; height: 200px; width: 200px;');
    }
    else{
	chart.setAttribute('style', 'display: block !important; height: 210px; width: 210px; padding: 20px;');
    }
});


var tci_t35 = document.getElementById("t35_tci_checkbox");
//var tci_t34 = document.getElementById("t34_tci_checkbox");

var clc_checkbox = document.getElementById("clc_checkbox");
var checkbox_2016 = document.getElementById("checkbox_2016");
var checkbox_2017 = document.getElementById("checkbox_2017");
//var checkbox_2018 = document.getElementById("checkbox_2018");
var checkbox_t35_prediction = document.getElementById("checkbox_t35_prediction")

function addEventForCheckboxAddLayer(checkbox, layer){
    checkbox.addEventListener('change', function() {
	if(this.checked) {

	        if (Array.isArray(layer)){
	            for (var i=0; i<layer.length;i++){
	                map.addLayer(layer[i]);
                }
            }else {

                map.addLayer(layer);
            }
	} else {

	    if (Array.isArray(layer)){

	            for (var i=0; i<layer.length;i++){
	                map.removeLayer(layer[i]);
                }
            }else
        {

            map.removeLayer(layer);
        }
	}
    });    
}

//addEventForCheckboxAddLayer(clc_checkbox, forest_example);
addEventForCheckboxAddLayer(tci_t35, [mosaic_t35_tci,mosaic_t34_tci]);
//addEventForCheckboxAddLayer(tci_t34, mosaic_t34_tci);

addEventForCheckboxAddLayer(checkbox_2016, T35TML_2016_prediction);
addEventForCheckboxAddLayer(checkbox_2017, T35TML_2017_prediction);
addEventForCheckboxAddLayer(checkbox_2018, T35TML_2018_prediction);

addEventForCheckboxAddLayer(checkbox_t35_prediction, prediction_mosaic);


document.getElementById('t35_tci_checkbox').checked = true;
