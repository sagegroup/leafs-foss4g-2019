import os
import numpy as np
from multiprocessing import Pool
import fiona
from fiona.transform import transform_geom
import rasterio
import rasterio.features
from tqdm import tqdm


def generate_gti(s_tile):
    feature_vector = {
        'T33': '/storage/syno1/forestry-segmentation/scripts/biggeraoi_features_33.shp',
        'T34': '/storage/syno1/forestry-segmentation/scripts/biggeraoi_features_34.shp',
        'T35': '/storage/syno1/forestry-segmentation/scripts/biggeraoi_features_35.shp',
    }

    # vec = feature_vector[s_tile[:3]]
    vec = '/data/syno1/sage-storage/forestry-segmentation/corine/clc_full.shp'
    # out_dir = '/storage/syno1/forestry-segmentation/dataset/GTI/'
    out_dir = '/data/syno1/sage-storage/forestry-segmentation/dataset/bigearth-test/'
    f = fiona.open(vec)
    f_profile = f.meta
    # f_profile['driver'] = 'ESRI Shapefile'
    f_profile['driver'] = 'FileGDB'
    # sentinel_tile = rasterio.open('/storage/syno1/forestry-segmentation/dataset/MUL/' + s_tile)
    sentinel_tile = rasterio.open(s_tile)
    already_has_features = False

    mapping = {
        999: {'value': 0},    # No data
        243: {'value': 1},    # Land principally occupied by agriculture
        244: {'value': 2},    # Agro-forestry areas
        311: {'value': 3},    # Broad-leaved forest
        312: {'value': 4},    # Coniferous forest
        313: {'value': 5},    # Mixed forest
        321: {'value': 6},    # Natural grasslands
        322: {'value': 7},    # Moors and heathland
        323: {'value': 8},    # Sclerophyllous vegetation
        324: {'value': 9}}    # Transitional woodland-shrub

    profile = sentinel_tile.profile
    profile.update(
        dtype=rasterio.uint8,
        count=1,
        driver='GTiff',
        compress='lzw')

    my_features = []
    for feature in tqdm(f):
        geom = feature['geometry']
        prop = feature['properties']
        code_18 = int(prop['Code_18'])

        if code_18 not in mapping:
            continue

        if not already_has_features:
            # new_feature = transform_geom(f.crs['init'],
            # print("Converting features from %s to %s CRS" % ('espg:3035', sentinel_tile.crs))
            new_feature = transform_geom('epsg:3035',
                                         str(sentinel_tile.crs),
                                         geom)

            feature['geometry'] = new_feature
            my_features.append(feature)

        if 'geoms' not in mapping[code_18]:
            mapping[code_18]['geoms'] = []
            geoms = mapping[code_18]['geoms']

        if not already_has_features:
            geoms.append(new_feature)
        else:
            geoms.append([feature['geometry'], mapping[code_18]['value']])

    # if not already_has_features:
    #     fname = 'biggeraoi_features_35.shp'
    #     print("Writing output features to %s" % fname)
    #     with fiona.open(fname, 'w', **f_profile) as features_file:
    #         for feature in my_features:
    #             features_file.write(feature)

    # writing raster from vector data
    with rasterio.Env():
        arr = np.zeros(sentinel_tile.shape, dtype=np.uint8)

        fname = out_dir + s_tile.split('/')[-1].split('.')[0] + '_GTI.tif'
        with rasterio.open(fname, 'w', **profile) as dst:
            for code_18, entry in mapping.items():
                value = entry['value']
                geoms = entry.get('geoms', [])

                if not geoms:
                    print("Ignoring %s as it does not contain any geoms" % code_18)
                    continue


                print("Burning code_18: %s with value: %d" % (code_18, value))
                rasterio.features.rasterize(geoms,
                                            out_shape=sentinel_tile.shape,
                                            fill=0,
                                            default_value=0,
                                            transform=dst.transform,
                                            out=arr)

                entry['geoms'] = []
                del geoms
                dst.write(arr.astype(rasterio.uint8), 1)


# generate_gti('/storage/syno1/forestry-segmentation/dataset/MUL/T33TXK_20180823T094029_B02_10.jp2')
# generate_gti('/storage/syno1/forestry-segmentation/dataset/MUL/T33TXL_20180828T094031_B02_10.jp2')

# files = os.listdir('/storage/syno1/forestry-segmentation/dataset/MUL/')

# with Pool(processes=10) as pool:
#     pool.map(generate_gti, files)

generate_gti('/data/syno1/sage-storage/forestry-segmentation/dataset/bigearth-test/S2B_MSIL2A_20180422T093029_24_65/S2B_MSIL2A_20180422T093029_24_65_B02_wgs84.tif')
