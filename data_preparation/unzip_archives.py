import os
from tqdm import tqdm
from zipfile import ZipFile

zips_path = '/data/syno1/sage-storage/forestry-segmentation/dhus_products/'
target_dir = '/data/syno1/sage-storage/forestry-segmentation/dataset/MUL-test/'

# zips_path = '/home/alex/facultate/ieat/forestry-segmentation/scripts/'
# target_dir = '/home/alex/facultate/ieat/forestry-segmentation/scripts/extracted_bands/'


def get_res(band):
    bands_and_res = {'B02': '10', 'B03': '10', 'B04': '10', 'B08': '10', 'B05': '20', 'B06': '20', 'B07': '20', 'B8A': '20', 'B11': '20', 'B12': '20', 'B01': '60', 'B09': '60', 'B10': '60'}
    return bands_and_res[band]


for _, _, files in os.walk(zips_path):
    zips = [file for file in files if file.endswith('.zip')]
    break


for zip_path in tqdm(zips):
    with ZipFile(zips_path + zip_path, 'r') as zipf:
        files = [file for file in zipf.namelist() if file.endswith('.jp2') and 'IMG_DATA' in file and 'TCI' not in file]

        for file in files:
            file_tokens = file.split('/')[-1].split('.')[0].split('_')
            # ZipFile doesn't support file-targeted extraction, so we open and write to the file we want.
            with open(target_dir + file_tokens[0] + '_' + file_tokens[1] + '_' + file_tokens[2] + '_' + get_res(file_tokens[2]) + '.jp2', 'wb') as f:
                f.write(zipf.read(file))
