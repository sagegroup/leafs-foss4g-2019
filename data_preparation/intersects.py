import os
import ogr
import osr
import gdal
import fiona
from fiona.transform import transform_geom


def transform_shapefile_features(shapefile, dst_crs):
    features = []
    with fiona.open(shapefile) as shpfile:
        for feature in shpfile:
            new_feature = transform_geom(shpfile.crs['init'],
                                         dst_crs,
                                         feature['geometry'])
            feature['geometry'] = new_feature
            features.append(feature)

        print("Writing shapefile in new projection")
        with fiona.open('features_epsg32634.shp', 'w', **shpfile.meta) as features_file:
            for feature in features:
                features_file.write(feature)


def check_if_intersects(sentinel_tile, shapefile):
    sentinel_tile = gdal.Open('/storage/syno1/forestry-segmentation/dataset/MUL/' + sentinel_tile)
    sentinel_tile_transform = sentinel_tile.GetGeoTransform()
    pxwidth = sentinel_tile_transform[1]
    pxheight = sentinel_tile_transform[5]
    columns = sentinel_tile.RasterXSize
    rows = sentinel_tile.RasterYSize

    left = sentinel_tile_transform[0]
    top = sentinel_tile_transform[3]
    right = left + columns * pxwidth
    bottom = top - rows * pxheight

    # print(left, top, right, bottom)
    raster_shape = ogr.Geometry(ogr.wkbLinearRing)
    raster_shape.AddPoint(left, top)
    raster_shape.AddPoint(left, bottom)
    raster_shape.AddPoint(right, bottom)
    raster_shape.AddPoint(right, top)
    raster_shape.AddPoint(left, top)
    final_raster_geom = ogr.Geometry(ogr.wkbPolygon)
    final_raster_geom.AddGeometry(raster_shape)

    features = shapefile.GetLayer()
    # print(features.GetFeatureCount())
    feature = features.GetFeature(0)
    feature_geom = feature.GetGeometryRef()

    src_crs = osr.SpatialReference().ImportFromEPSG(323634)
    dst_crs = osr.SpatialReference().ImportFromEPSG(3035)

    print(feature_geom)
    feature_geom.Transform(osr.CoordinateTransformation(src_crs, dst_crs))

    # print(final_raster_geom, feature_geom)

    print(final_raster_geom.Intersects(feature_geom))
    #     print(sentinel_tile)
    # else:
    #     print("Tile %s does not intersect features polygon" % sentinel_tile)

    del sentinel_tile


gtis = os.listdir('/storage/syno1/forestry-segmentation/dataset/MUL/')
features = ogr.Open('/storage/syno1/forestry-segmentation/corine/features.shp')
# features = ogr.Open('/storage/syno1/forestry-segmentation/corine/features_epsg32634.shp')

# print(gtis)

counter = 0
for gti in gtis:
    # if counter <= 20:
        # print(gti)
    check_if_intersects(gti, features)
        # counter += 1


# transform_shapefile_features('/storage/syno1/forestry-segmentation/corine/features.shp', 'epsg:32634')
