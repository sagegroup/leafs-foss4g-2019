import rasterio
from zipfile import ZipFile

target_zip = 'S2B_MSIL1C_20180829T100019_N0206_R122_T33UXP_20180829T173115.zip'
# possible id?
# {mission_identifier}_{file_type}_{sensing_start}_{rel_orbit}_{tile_id}_{sensing_end}

with ZipFile(target_zip, 'r') as zipf:
    files = [file for file in zipf.namelist() if file.endswith('.jp2')]
    file_name = target_zip.split('_')
    file_name = file_name[5] + '_' + file_name[6].split('.')[0]

    for file in files:
        rasterio.open('/vsizip/' + target_zip + '/' + file)
