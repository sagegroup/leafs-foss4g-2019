import os
import numpy as np
import rasterio as rio


files = os.listdir('/home/alex/facultate/ieat/forestry-segmentation/dataset/MUL/')
files = [file for file in files if 'B02' in file]

for file in files:
    raster = rio.open('/home/alex/facultate/ieat/forestry-segmentation/dataset/MUL/' + file)
    data = raster.read()

    if len(data[data == 0]) <= 24112080:
        print("%s contains %d 0-pixels" % (file ,(len(data[data == 0]))))
