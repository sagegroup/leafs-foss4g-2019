# LeafS #

This is the repository for team LeafS, participating at FOSS4G EO Data Challenge 2019.

![LefS - LEverage Artificial Intelligence for Forest Sustainability](https://drive.google.com/uc?export=view&id=1qlIXkose25tSlrdUhAChf-KAu4__1GY1)


## Content


* data_preparation - contains script for downloading and pre-processing the input data:
	* 	Download Sentinel-2 data from DHUS for Romania AOI
	*  Process Corine Land Cover (CLC) to extract corresponding Ground Truth Image (GTI) for the learning algorithms 
* models - contains the model implementations that were used for identifying forest pattern; we have also used FCDenseNet67 model (already implemented in Keras); we present our implementation of the following models
	* U-Net
	* HSN
	* W-Net
* webapp - contains the web application interface files based on :
	* OpenLayers
	* GeoServer
	* Twitter Bootrasp
	* Parcel

## Technologies used

For our deep learning implementation we have used the open-source pipeline [Hugin](https://github.com/sage-group/hugin), to which that the authors of this project (LeafS) are cotributing to develop from the incipient phase. 

Hugin is a Python 3 Deep Learning pipeline than includes technologies as:

* Keras with Tensorflow backend
* Scikit
* OpenCV
* RasterIO

![U-Net Sample on model learning](https://drive.google.com/uc?export=view&id=17St2DqfsyIqQ7RG7dGe-4pjXagS9MZlC)

## Contact

For further information, do not hesitate to contact teodora.selea@e-uvt.ro and alexandru.munteanu98@e-uvt.ro

Thank you,

Teodora and Alex