import os
import rasterio


files = os.listdir('/storage/syno1/forestry-segmentation/dataset/MUL')
print(len(files))

min_x = 100
min_y = 100
max_x = 0
max_y = 0

for file in files:
    with rasterio.open('/storage/syno1/forestry-segmentation/dataset/MUL/' + file) as f:
        coords = f.lnglat()
        if coords[0] < min_x:
            min_x = coords[0]
        elif coords[0] > max_x:
            max_x = coords[0]

        if coords[1] < min_y:
            min_y = coords[1]
        elif coords[1] > max_y:
            max_y = coords[1]


print(f"min_x: {min_x}, max_x: {max_x}, min_y: {min_y}, max_y: {max_y}")
# min_x: 16.953255142475026, max_x: 26.439252285056202, min_y: 43.71532032877245, max_y: 49.157238705287504
